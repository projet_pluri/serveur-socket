#include "connexion.h"


int init_connexion(int *sockfd, int *connectefd){
	int listenfd=0, connfd=0;
	
	struct sockaddr_in serv_addr; 

	if((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		perror("init()");
		return -1;
	}
	
	memset(&serv_addr, '0', sizeof(serv_addr));/*initialise les champs à 0*/
    /*memset(sendBuff, '0', sizeof(sendBuff)); */

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(PORT); 

	if(bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
		perror("bind()");
		return -1;
	}

	if(listen(listenfd, 2) < 0){
		perror("listen()");
		return -1;
	}


	if((connfd = accept(listenfd, (struct sockaddr*)NULL, NULL)) < 0){/*attente de connexion du client*/
		perror("accept()");
	}
	/*snprintf(sendBuff, sizeof(sendBuff), "début com\n");
	write(connfd, sendBuff, strlen(sendBuff));*/
	
	/*pseudos returns*/
	*sockfd=listenfd;
	*connectefd=connfd;
	
	return 0;
}


void attente_datas(int connectefd, zone tab_zone[4]){
/* 	test my_test;
 	recv(connectefd, (void*)&my_test, sizeof(test), 0);*/

	recv(connectefd, (void*)tab_zone, 4*sizeof(zone),0);
	
	/*return my_test;*/
}

void fin_connexion(int sockfd, int connectedfd){
	close(connectedfd);
	close(sockfd);
}