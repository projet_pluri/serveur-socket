#ifndef CONNEXION_H
#define CONNEXION_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>


#define PORT 5557

typedef struct zone zone;

#pragma pack(1)
struct zone{
	int nbPersonne;
	int distance;
};


int init_connexion(int *sockfd, int *connectefd);
void attente_datas(int connectefd, zone tab_zone[4]);

void fin_connexion(int sockfd, int connectedfd);

#endif