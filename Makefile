CC=gcc
CFLAGS=-W -Wall -Wextra -ansi -pedantic
SRC= $(wildcard *.c)
OBJ= $(SRC:.c=.o)
EXEC=serveur_socket

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $@ $^

connexion.o: connexion.h

%.o: %.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean mrproper

clean:
		rm -rf *.o

mrproper: clean
		rm -rf $(EXEC)

